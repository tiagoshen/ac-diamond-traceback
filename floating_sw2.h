/****
Copyright (c) 2014, University of Tuebingen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****
Author: Benjamin Buchfink
****/

#ifndef FLOATING_SW2_H_
#define FLOATING_SW2_H_

#include "../basic/match.h"
#include "dp_matrix2.h"
#include "score_vector2.h"
#include "score_profile2.h"
#include "../util/tinythread.h"
#include "traceback3.h"

using boost::thread_specific_ptr;

tthread::mutex output_mutex;

template<typename _score>
inline score_vector2<_score> cell_update2(const score_vector2<_score> &diagonal_cell,
					  const score_vector2<_score> &scores,
					  const score_vector2<_score> &gap_extension,
					  const score_vector2<_score> &gap_open,
					  const score_vector2<_score> &row,
					  score_vector2<_score> &horizontal_gap,
					  score_vector2<_score> &vertical_gap,
					  score_vector2<_score> &best,
					  score_vector2<_score> &row_best)
{
  score_vector2<_score> current_cell = diagonal_cell + scores;
  current_cell.max(vertical_gap).max(horizontal_gap);
  best.update_row(current_cell, row, row_best);
  vertical_gap -= gap_extension;
  horizontal_gap -= gap_extension;
  score_vector2<_score> open = current_cell - gap_open;
  vertical_gap.max(open);
  horizontal_gap.max(open);
  return current_cell;
}   

template<typename _val, typename _score>
void floating_sw_suffix(vector<sequence<const _val> > &ref_suffix_set,
			sequence<const _val> query_suffix,
			int band,
			int xdrop,
			int gap_open,
			int gap_extend,
			int begin,
			vector<local_match<_val> > &segments,
			score_vector2<_score> *traceback,
			vector<char> &transcript_buf)
{
  typedef score_vector2<_score> sv;
  
  int slen (ref_suffix_set[0].length());
  int qlen (query_suffix.length());
  DP_matrix2<_score> dp (slen, qlen, band);

  sv open_penalty (gap_open);
  sv extend_penalty (gap_extend);
  sv drop_score (xdrop);
  sv temp;
  int temp2, i, j, n = 0;
  int pos, width, odd;
  width = 2 * band + 2;
  
  sequence_stream2 dseq_ref;
  score_profile2<_score> profile;
  
  typename vector<sequence<const _val> >::const_iterator ref_begin (ref_suffix_set.begin());
  
  while (ref_begin < ref_suffix_set.end()){
    const int n_ref (std::min((int)score_traits2<_score>::channels, (int)(ref_suffix_set.end() - ref_begin)));
    typename vector<sequence<const _val> >::const_iterator ref_end (ref_begin + n_ref);
    
    dseq_ref.reset();
    dp.clear2();
    sv best, best_row, best_column, column_score_max;
        
    for (j = 0; j < slen; j++){
      typename DP_matrix2<_score>::Column_iterator it (dp.begin(j));
      sv vgap, hgap, row_max;
      sv column (j);
      column_score_max = sv ();
      pos = (j+1)*width;
      
      odd = j % 2;
      if (odd == 0){
        __m128i thisseq = dseq_ref.get_right<_val>(ref_begin, ref_end, j, _score());
	__m128i nextseq = dseq_ref.get_right<_val>(ref_begin, ref_end, j + 1, _score());
	profile.template set<_val> (thisseq, nextseq);

	while (!it.at_end()){
	  pos++;
	  hgap = it.hgap();
	  sv row ((int)it.row_pos_);
	  traceback[pos] = cell_update2<_score>(it.diag(), profile.get(query_suffix[it.row_pos_]), extend_penalty, open_penalty, row, hgap, vgap, column_score_max, row_max);
	  it.set_hgap(hgap);
	  it.set_score(traceback[pos]);
	  ++it;
	}
      }
      else {
	while (!it.at_end()){
	  pos++;
	  hgap = it.hgap();
	  sv row ((int)it.row_pos_);
	  traceback[pos] = cell_update2<_score>(it.diag(), profile.get_next(query_suffix[it.row_pos_]), extend_penalty, open_penalty, row, hgap, vgap, column_score_max, row_max);
	  it.set_hgap(hgap);
	  it.set_score(traceback[pos]);
	  ++it;
	}
      }
      // check whther to stop
      temp = (best - column_score_max).cmpgt(drop_score);
      temp2 = 0;
      for (i = 0; i < n_ref; i++){
	if (temp[i] == -1) temp2++;
      }
      if (temp2 == n_ref) break;
      
      best.update(column_score_max, row_max, column, best_row, best_column);
    }
    
    dp_traceback_right<_val, _score>(segments, ref_suffix_set, n, query_suffix, traceback, n_ref, best_row.data_, best_column.data_, best.data_, gap_open, gap_extend, band, begin, transcript_buf);
    
    ref_begin += score_traits2<_score>::channels;
    n += score_traits2<_score>::channels;
  }
}

template<typename _val, typename _score>
void floating_sw_prefix(vector<sequence<const _val> > &ref_prefix_set,
			sequence<const _val> query_prefix,
			int band,
			int xdrop,
			int gap_open,
			int gap_extend,
			int begin,
			vector<local_match<_val> > &segments,
			score_vector2<_score> *traceback,
			vector<char> &transcript_buf)
{
  typedef score_vector2<_score> sv;
  
  int slen (ref_prefix_set[0].length());
  int qlen (query_prefix.length());
  DP_matrix2<_score> dp (slen, qlen, band);
  
  sv open_penalty (gap_open);
  sv extend_penalty (gap_extend);
  sv drop_score (xdrop);
  sv temp;
  int temp2, i, j, n = 0;
  int pos, width, odd;
  width = 2 * band + 2;

  sequence_stream2 dseq_ref;
  score_profile2<_score> profile;

  typename vector<sequence<const _val> >::const_iterator ref_begin (ref_prefix_set.begin());

  while (ref_begin < ref_prefix_set.end()){
    const int n_ref (std::min((int)score_traits2<_score>::channels, (int)(ref_prefix_set.end() - ref_begin)));
    typename vector<sequence<const _val> >::const_iterator ref_end (ref_begin + n_ref);
    
    dseq_ref.reset();
    dp.clear2();
    sv best, best_row, best_column, column_score_max;
    
    for (j = 0; j < slen; j++){
      typename DP_matrix2<_score>::Column_iterator it (dp.begin(j));
      sv vgap, hgap, row_max;
      sv column (j);
      column_score_max = sv();
      pos = (j+1)*width;
      odd = j % 2;

      if (odd == 0){
	__m128i thisseq = dseq_ref.get_left<_val>(ref_begin, ref_end, j, _score());
	__m128i nextseq = dseq_ref.get_left<_val>(ref_begin, ref_end, j + 1, _score());
	profile.template set<_val> (thisseq, nextseq);
      
	while (!it.at_end()){
	  pos++;
	  hgap = it.hgap();
	  sv row ((int)it.row_pos_);
	  traceback[pos] = cell_update2<_score>(it.diag(), profile.get(query_prefix[qlen-it.row_pos_-1]), extend_penalty, open_penalty, row, hgap, vgap, column_score_max, row_max);
	  it.set_hgap(hgap);
	  it.set_score(traceback[pos]);
	  ++it;
	}
      }
      else {
	while (!it.at_end()){
	  pos++;
	  hgap = it.hgap();
	  sv row ((int)it.row_pos_);
	  traceback[pos] = cell_update2<_score>(it.diag(), profile.get_next(query_prefix[qlen-it.row_pos_-1]), extend_penalty, open_penalty, row, hgap, vgap, column_score_max, row_max);
	  it.set_hgap(hgap);
	  it.set_score(traceback[pos]);
	  ++it;
	}
      }
      // check whther to stop 
      temp = (best - column_score_max).cmpgt(drop_score);
      temp2 = 0;
      for (i = 0; i < n_ref; i++){
        if (temp[i] == -1) temp2++;
      }
      if (temp2 == n_ref) break;

      best.update(column_score_max, row_max, column, best_row, best_column);
    }

    dp_traceback_left<_val, _score>(segments, ref_prefix_set, n, query_prefix, traceback, n_ref, best_row.data_, best_column.data_, best.data_, gap_open, gap_extend, band, begin, transcript_buf);

    ref_begin += score_traits2<_score>::channels;
    n += score_traits2<_score>::channels;
  }
}

template<typename _val, typename _score>
void floating_sw2(vector<sequence<const _val> > &ref_suffix_set,
		  vector<sequence<const _val> > &ref_prefix_set,
		  sequence<const _val> query_suffix,
		  sequence<const _val> query_prefix,
		  int band,
		  int xdrop,
		  int gap_open,
		  int gap_extend,
		  int begin,
		  vector<local_match<_val> > &segments,
		  score_vector2<_score> *traceback,
		  vector<char> &transcript_buf,
		  const _score&)
{
  // suffixes
  floating_sw_suffix<_val, _score>(ref_suffix_set, query_suffix, band, xdrop, gap_open, gap_extend, begin, segments, traceback, transcript_buf);
  // prefixes
  floating_sw_prefix<_val, _score>(ref_prefix_set, query_prefix, band, xdrop, gap_open, gap_extend, begin, segments, traceback, transcript_buf);
}

#endif /* FLOATING_SW2_H_ */
