#ifndef TRACEBACK2_H_
#define TRACEBACK2_H_

#include "score_vector3.h"

template<typename _score>
struct Traceback_matrix
{
        Traceback_matrix(score_vector2<_score> *scores, int band, int len):
        //scores是所有（最多8个）DP table的指针
        scores_ (scores),
        //band_和width_
		band_ (score_vector2<_score> (band+1)),
		width_ (score_vector2<_score> (2*band+2)),
		ref_len (len),
		
		//在SIMD中不用，代表第num个ref_sequence
		//num_ (num)
	{ }
		
		//根据col和row从scores中返回新的score_vector2
        score_vector2<_score> operator()(const score_vector2<_score> &col, const score_vector2<_score> &row) const {
	  		//对应原来的代码：int top_delta (col > band_ ? col - band_ : 0);
	  		score_vector2<_score> top_delta = ((col - band).cmpgt(zero).mask())*(col-band);

	  		//mask：相当于一个boolean vector, true时为1，false时为0
	  		score_vector2<_score> bmask = in_band(col, row).mask();

	  		//对应原来的代码：int pos = col*width_+row-top_delta;
	  		score_vector2<_score> pos = (col*width_+row-top_delta)*bmask;

	  		//需要返回的score_vector2<_score>,这里将sv中的每个元素单独赋值，没有用到SIMD
	  		score_vector2<_score> re (0);
	  		for (unsigned int i=0; i<ref_len; i++){
	  			re.set(i, scores_[pos[i]][i]);
	  			if (pos[i]==0) re.set(i, 0);
	  		}
	  		
	  		return re;
		}
        
        //返回的是boolean vector，true是1，false是0
        score_vector2<_score> in_band(score_vector2<_score> &col, score_vector2<_score> &row) const
        {
	  		//原来的代码：return abs(col - row) < band_ && row > 0 && col > 0;
	  		return (col-row).abs().cmplt(band_).vand(row.cmpgt(zero)).vand(col.cmpgt(zero))；
		} 
        /*void print(int col, int row) const
	{
		for(unsigned j=0;j<=row;++j) {
			for(unsigned i=0;i<=col;++i)
				printf("%4i", in_band(i, j) ? this->operator()(i, j) : 0);
			printf("\n");
		}
		}*/
private:
	const score_vector2<_score> *scores_;
	const score_vector2<_score> band_;
    const score_vector2<_score> width_;
    const score_vector2<_score> ref_len;
};

template<typename _score>
//i代表各自的行，j代表各自的列，返回boolean vector
score_vector2 have_vgap(Traceback_matrix<_score> &dp,
	       const score_vector2<_score> &i,				//i在调用过程中有改动，应该拷贝值
	       const score_vector2<_score> &j,				//j不改动,所以不用拷贝
	       const score_vector2<_score> &gap_open,		//8个都一样
	       const score_vector2<_score> &gap_extend,		//8个都一样	
	       score_vector2<_score> &l,					//gap_len
	       int band)									
{
	const score_vector2<_score> one (1);
    score_vector2<_score> score = dp(j, i);
    
    //将向量l中的每个元素赋值为1，对应非SIMD的逻辑：l = 1
	l -= l;
	++l;

	//r是i的拷贝
	score_vector2<_score> r (i.data_);
	--r;
	
	score_vector2<_score> mask (0);
	while(true) {
	    
	    //对应非SIMD的逻辑: while(dp.in_band(j, i))
	    score_vector2<_score> bmask = dp.in_band(j, r);
	    if (bmask.allzero()) break;
	    
	    //对应非SIMD的逻辑:
	    //if((int)score == (int)dp(j, r) - gap_open - (l-1)*gap_extend)
		//return true;
		mask = score.cmpeq(dp(j, r) - gap_open - (l-one)*gap_extend).vand(bmask).vor(mask);
		--r;
		++l;
	}
	return mask;
}


template<typename _score>
//i代表各自的行，j代表各自的列，返回boolean vector
score_vector2<_score> have_hgap(Traceback_matrix<_score> &dp,
	       const score_vector2<_score> &i,				//i不改动,所以不用拷贝
	       const score_vector2<_score> &j,				//j在调用过程中有改动，应该拷贝值
	       const score_vector2<_score> &gap_open,		//8个都一样
	       const score_vector2<_score> &gap_extend,		//8个都一样	
	       score_vector2<_score> &l,					//gap_len
	       int band)
{
	const score_vector2<_score> one (1);
    score_vector2<_score> score = dp(j, i);

    //将向量l中的每个元素赋值为1，对应非SIMD的逻辑：l = 1
	l -= l;
	++l;

	//r是j的拷贝
	score_vector2<_score> r (j.data_);
	--r;

	score_vector2<_score> mask (0);
	while(true) {
		
		//对应非SIMD的逻辑: while(dp.in_band(j, i))
		score_vector2<_score> bmask = dp.in_band(r, i);
		if (bmask.allzero()) break;

	    //对应非SIMD的逻辑:
	    //if((int)score == (int)dp(r, i) - gap_open - (l-1)*gap_extend)
		//return true;
	    mask = score.cmpeq(dp(r, i) - gap_open - (l-one)*gap_extend).vand(bmask).vor(mask)
		--r;
		++l;
	}
	return mask;
}

//使用了SIMD，但是有改进的空间：将所有对score_vector2<_score>类实例进行的运算，都改为直接对__m128i向量进行运算，避免了使用类，也许会提高速度并减少内存占用，我后面会尝试这个改进方法
template<typename _val, typename _score>
void dp_traceback_right(vector<local_match<_val>> &segment,
			sequence<const _val> &ref_suffix,
			sequence<const _val> &query_suffix,
			score_vector2<_score> *scores,	//在floating_sw2.h中是traceback
			int reflen,						//替换掉num，这里reflen是reference sequence的"数量"1-8个
			score_vector2<_score> &i,		//在floating_sw2.h中是best_row
			score_vector2<_score> &j,		//在floating_sw2.h中是best_column
			score_vector2<_score> &best,	//在floating_sw2.h中是best
			//这一部分没有变
			int gap_open,
			int gap_extend,
			int scalar_band,				//原来是band,改了名字

			int begin,						//在floating_sw2.h中是begin用来取segment中相应位置的元素
			vector<char> &transcript_buf)
{
    //if (j < 0 || i < 0) return;	这个没有implement
    
    //几乎所有sv都可直接用__m128i替换
    typedef score_vector2<_score> sv;
    
    //将一些常量初始化为向量，后面需要用来计算
	const sv one (1);		//每个16bits整数均为1
	const sv zero (0);		//每个16bits整数均为0,等价于: const score_vector2<_score> zero (false);
	const sv ones (true);	//每个16bits整数均为-1, i.e. 0xffff
    
    sv gopen (gap_open);
    sv gextend (gap_extend);
    sv band (scalar_band);
    sv ref_len (reflen);

	Traceback_matrix<_score> dp (scores, band, reflen);
	
	//这里需要将segment的各个需要的元素构建成vector
	/*
  	segment.query_len_ = i + 1;
  	segment.subject_len_ = j + 1;
  	segment.query_begin_ = 0;
  	segment.subject_begin_ = 0;
  	segment.score_ = best;
  	*/
  	sv query_len ((i+one).data_);
  	sv subject_len ((j+one).data_);
  	sv query_begin (0);
  	sv subject_begin (0);
  	sv score (best.data_);
  	sv identities (0);
  	sv mismatches (0);
  	sv len (0);
  	sv gap_openings (0);
  	
	Edit_transcript transcript (transcript_buf);
 
	sv gap_len (1);

	//0对应op_insertion, a>0对应op_insertion, a = gap_len, b<0对应op_deletion, |b| = gap_len 
	vector<vector<short int>> operations (reflen);
	
	i++;
	j++;

	bool endloop = false;
	while(!endloop) {		//i>1 || j>1
	    //需要读取所有match_score
	    score_vector2<_score> match_score (0);
	    for (unsigned int index=0; index<reflen; index++){
	    	match_score.set(index, score_matrix::get().letter_score(query_suffix[i[index]-1], mask_critical(ref_suffix[j[index]-1])));
	    }

		//printf("i=%i j=%i score=%i subject=%c query=%c\n",i,j,dp(j, i),Value_traits<_val>::ALPHABET[mask_critical(ref_suffix[j])],Value_traits<_val>::ALPHABET[query_suffix[i]]);
		/*if(i == 0 || j == 0) {
		        if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, qlen, band)) {
			        ++segment.gap_openings_;
				segment.len_ += gap_len;
				j -= gap_len;
				transcript_buf.insert(transcript_buf.end(), gap_len ,op_deletion);
			} else if (have_vgap(dp, i, j, gap_open, gap_extend, gap_len, qlen, band)) {
			        ++segment.gap_openings_;
				segment.len_ += gap_len;
				i -= gap_len;
				transcript_buf.insert(transcript_buf.end(), gap_len ,op_insertion);
			}
			} else*/

	    //主要流程
	    //创建一个以以上条件判断为结果的boolean vector (mask)，true为1，false为0
	    //对应非SIMD逻辑：if(dp(j, i) == (_score)match_score + dp(j-1, i-1))
				score_vector2<_score> mask1 = dp(j, i).cmpeq(match_score + dp(j-one, i-one)).mask();

			  score_vector2<_score> ident (0);

			  //对应非SIMD逻辑：if(query_suffix[i-1] == mask_critical(ref_suffix[j-1]))
			  for (unsigned int index=0; index<reflen; index++){
		      	if(query_suffix[i[index]-1] == mask_critical(ref_suffix[j[index]-1]))
		        	ident.set(index, 1);
		      }
		      identities += ident;
		      mismatches += (one-ident);
			  i -= mask1;	//i--
			  j -= mask1;	//j--
			  len += mask1; //segment.len_++
			  
			  //只能通过for循环来添加信息，无法通过SIMD完成这个任务
			  //transcript_buf.push_back(op_match);
			  for (unsigned int index=0; index<reflen; index++){
			  	if (mask1[index] == 1) operations[index].push_back(0);
			  }

		//对应非SIMD逻辑：else if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, band)) 
		score_vector2<_score> mask2 = have_hgap(dp, i, j, gopen, gextend, gap_len, band).mask();
			/*
			++segment.gap_openings_;
			segment.len_ += gap_len;
			j -= gap_len;
			transcript_buf.insert(transcript_buf.end(), gap_len, op_deletion);
			*/
			gap_openings_ += mask2;
			len += gap_len*mask2;
			j -= gap_len*mask2;
			for (unsigned int index=0; index<reflen; index++){
			  	if (mask2[index] == 1) operations[index].push_back(-gap_len[index]);
			}

		score_vector2<_score> mask3 = have_vgap(dp, i, j, gopen, gextend, gap_len, band).mask();
			/*
			++segment.gap_openings_;
			segment.len_ += gap_len;
			i -= gap_len;
			transcript_buf.insert(transcript_buf.end(), gap_len, op_insertion);
			*/
			gap_openings_ += mask3;
			len_ += gap_len*mask3;
			i -= gap_len*mask3;
			for (unsigned int index=0; index<reflen; index++){
			  	if (mask3[index] == 1) operations[index].push_back(gap_len[index]);
			}

		//else throw std::runtime_error("Traceback error."); 没有implement

		//while(i>1 || j>1)
		endloop = (i.cmpgt(one).allzero() || j.cmpgt(one).allzero());
	}

	/*
	if(query_suffix[0] == mask_critical(ref_suffix[0]))
		++segment.identities_;
	else
		++segment.mismatches_;
	++segment.len_;
	transcript_buf.push_back(op_match);
	*/
	score_vector2<_score> mask4 (0);
	for (index=0; index<reflen; index++){
		if(query_suffix[0] == mask_critical(ref_suffix[0]))
		    mask4[index]=1;
	}
	identities += mask4;
	mismatches -= mask4-one;
	len += mask4;
	for (unsigned int i=0; i<reflen; i++){
		if (mask4[i] == 1) operations[i].push_back(0);
	}

	//将所有输出数据汇总
	for (index=begin; index<reflen+begin; index++){
		segment[index].query_len_ = query_len[index];
  		segment[index].subject_len_ = subject_len[index];
  		//No use?
  		segment[index].query_begin_ = query_begin[index];
  		segment[index].subject_begin_ = subject_begin[index];

  		segment[index].score_ = score[index];
  		segment[index].identities_ = identities[index];
  		segment[index].mismatches_ = mismatches[index];
  		segment[index].len_ = len[index];
  		segment[index].gap_openings_ = gap_openings[index];
  		for(vector<short int>::iterator it=operations[index].begin();it!=operations[index].end();++it){
			if (*it==0){
				transcript_buf.push_back(op_match);
			}else if (*it>0){
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_insertion);
			}else{
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_deletion);
			}
		}
		//printf("len=%i\n",segment.len_);
		segment[index].transcript_right_ = transcript.set_end(transcript_buf);
	}

}


//未改
template<typename _val, typename _score>
void dp_traceback_left(local_match<_val> &segment,
		       sequence<const _val> &ref_prefix,
		       sequence<const _val> &query_prefix,
		       score_vector2<_score> *scores,
		       unsigned num,
		       int i,
		       int j,
		       _score best,
		       int gap_open,
		       int gap_extend,
		       int band,
		       vector<char> &transcript_buf)
{
        if (j < 0 || i < 0) return;

	Traceback_matrix<_score> dp (scores, band, num);
	
	local_match<_val> segment_left;
	segment_left.query_len_ = i + 1;
	segment_left.subject_len_ = j + 1;
	segment_left.query_begin_ = 0;
	segment_left.subject_begin_ = 0;
	segment_left.score_ = best;
	Edit_transcript transcript (transcript_buf);
	
	int gap_len;
	int slen = ref_prefix.length();
        int qlen = query_prefix.length();
	i++;
	j++;

        while(i>1 || j>1) {
	        //cout << dp(j,i) << endl;
	        const int match_score = score_matrix::get().letter_score(query_prefix[qlen-i], mask_critical(ref_prefix[slen-j]));
		//printf("i=%i j=%i score=%i subject=%c query=%c\n",i,j,dp(j, i),Value_traits<_val>::ALPHABET[mask_critical(ref_prefix[slen-j-1])],Value_traits<_val>::ALPHABET[query_prefix[qlen-i-1]]);
		/*if(i == 0 || j == 0) {
                        if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, qlen, band)) {
			        ++segment_left.gap_openings_;
				segment_left.len_ += gap_len;
                                j -= gap_len;
				transcript_buf.insert(transcript_buf.end(), gap_len ,op_deletion);
                        } else if (have_vgap(dp, i, j, gap_open, gap_extend, gap_len, qlen, band)) {
			        ++segment_left.gap_openings_;
                                segment_left.len_ += gap_len;
                                i -= gap_len;
				transcript_buf.insert(transcript_buf.end(), gap_len ,op_insertion);
			}
			} else*/
		if(dp(j, i) == (_score)match_score + dp(j-1, i-1)) {
		        if(query_prefix[qlen-i] == mask_critical(ref_prefix[slen-j]))
			        ++segment_left.identities_;
			else
			        ++segment_left.mismatches_;
			--i;
			--j;
			++segment_left.len_;
			transcript_buf.push_back(op_match);
                } else if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, band)) {
		        ++segment_left.gap_openings_;
			segment_left.len_ += gap_len;
			j -= gap_len;
			transcript_buf.insert(transcript_buf.end(), gap_len, op_deletion);
		} else if (have_vgap(dp, i, j, gap_open, gap_extend, gap_len, band)) {
		        ++segment_left.gap_openings_;
			segment_left.len_ += gap_len;
			i -= gap_len;
			transcript_buf.insert(transcript_buf.end(), gap_len, op_insertion);
                } else {
		        throw std::runtime_error("Traceback error.");
                }
        }
	
	if(query_prefix[qlen-1] == mask_critical(ref_prefix[slen-1]))
	        ++segment_left.identities_;
        else
	        ++segment_left.mismatches_;
        ++segment_left.len_;
        transcript_buf.push_back(op_match);
        //printf("len=%i\n",segment.len_);
        segment_left.transcript_right_ = transcript.set_end(transcript_buf);

	segment -= segment_left;
	segment.query_begin_--;
	segment.subject_begin_--;
	segment.score_ -= score_matrix::get().letter_score(query_prefix[qlen-1], mask_critical(ref_prefix[slen-1]));
	if (query_prefix[qlen-1] == mask_critical(ref_prefix[slen-1]))
	        segment.identities_--;
	else
	        segment.mismatches_--;
	segment.len_--;
	segment.subject_len_--;
	segment.query_len_--;	
}

/*template<typename _val, typename _score>
void dp_traceback(const _score &scores,
	     int gap_open,
	     int gap_extend,
	     int i,
	     int j,
	     int score)
{ return local_match<_val> (score); }
*/
#endif /* TRACEBACK2_H_ */