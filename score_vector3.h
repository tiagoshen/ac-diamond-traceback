/****
Copyright (c) 2014, University of Tuebingen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****
Author: Benjamin Buchfink
****/

#ifndef SCORE_VECTOR3_H_
#define SCORE_VECTOR3_H_

#ifdef __SSSE3__
#include <tmmintrin.h>

//新增的一个函数需要 SSE4
#include <smmintrin.h>

#endif

template<typename _score>
struct score_traits2
{
	static const unsigned channels = 1;
  enum { zero = 0, inf = -1024, byte_size = 4 };
	typedef bool Mask;
};

template<>
struct score_traits2<int16_t>
{
	//static const signed channels = 8;
  enum { channels = 8, zero = 0x0000, inf = 0xFC00, byte_size = 2/*, cons = 0xffff*/ };
  typedef int16_t Mask;
};

template<typename _score>
struct score_vector2
{ };

template<>
struct score_vector2<int16_t>
{

        score_vector2()
	{
	  data_ = _mm_set1_epi16(score_traits2<int16_t>::inf);
	}

	explicit score_vector2(int x):
		data_ (_mm_set1_epi16(x))
	{ }

	explicit score_vector2(__m128i data):
		data_ (data)
	{ }

	//新的构造器，b=true时所有八个整数均为-1 (0xffff,也就是每个bit均是1), b=false时所有八个整数均为0
	explicit score_vector2(bool b) {
		data_ = _mm_setzero_si128();
		if (b) data_ = _mm_cmpeq_epi16(data_, data_);
	}

	explicit score_vector2(int a, const __m128i &seq, const __m128i &nextseq)
	{
	        if(program_options::have_ssse3) {
#ifdef __SSSE3__
		        set_ssse3(a, seq, nextseq);
#else
			set_generic(a, seq, nextseq);
#endif
		} else
		        set_generic(a, seq, nextseq);
	}

	score_vector2 first_seq(const score_vector2 &rhs)
        {
	        if(program_options::have_ssse3) {
#ifdef __SSSE3__
		        first(rhs);
#else
			first_generic(rhs);
#endif
		} else
		        first_generic(rhs);
		return *this;
        }

	score_vector2 second_seq(const score_vector2 &rhs)
        {
	        if(program_options::have_ssse3) {
#ifdef __SSSE3__
		        second(rhs);
#else
			second_generic(rhs);
#endif
		} else
		        second_generic(rhs);
		return *this;
	}

	void set_ssse3(int a, const __m128i &seq, const __m128i &nextseq)
	{
#ifdef __SSSE3__
	        const __m128i *row = reinterpret_cast<const __m128i*>(&score_matrix::get().matrix8()[a << 5]);
		__m128i seq_8bits = _mm_packs_epi16(seq, nextseq);
                __m128i high_mask = _mm_slli_epi16(_mm_and_si128(seq_8bits, _mm_set1_epi8(0x10)), 3);
                __m128i seq_low = _mm_or_si128(seq_8bits, high_mask);
                __m128i seq_high = _mm_or_si128(seq_8bits, _mm_xor_si128(high_mask, _mm_set1_epi8(0x80)));
		__m128i r1 = _mm_load_si128(row);
		__m128i r2 = _mm_load_si128(row+1);
		__m128i s1 = _mm_shuffle_epi8(r1, seq_low);
		__m128i s2 = _mm_shuffle_epi8(r2, seq_high);
		data_ = _mm_or_si128(s1, s2);

	        //convert 16 bits to 8 bits
	        /*__m128i const zero = _mm_setzero_si128();
		__m128i seq_8bits = _mm_packs_epi16(seq, zero);
		__m128i high_mask = _mm_slli_epi16(_mm_and_si128(seq_8bits, _mm_set1_epi8(0x10)), 3);
		__m128i seq_low = _mm_or_si128(seq_8bits, high_mask);
		__m128i seq_high = _mm_or_si128(seq_8bits, _mm_xor_si128(high_mask, _mm_set1_epi8(0x80)));

		__m128i r1 = _mm_load_si128(row);
		__m128i r2 = _mm_load_si128(row+1);
		__m128i s1 = _mm_shuffle_epi8(r1, seq_low);
		__m128i s2 = _mm_shuffle_epi8(r2, seq_high);
		
		__m128i data_temp = _mm_or_si128(s1, s2);
		
		//convert 8 bits to 16 bits
		data_temp = _mm_unpacklo_epi8(zero, data_temp);
		data_ = _mm_srai_epi16(data_temp, 8);*/
#endif
	}

	void set_generic(unsigned a, const __m128i &seq, const __m128i &nextseq)
        {
	        const int8_t* row (&score_matrix::get().matrix8()[a<<5]);
		const int8_t* seq_ptr1 (reinterpret_cast<const int8_t*>(&seq));
		const int8_t* seq_ptr2 (reinterpret_cast<const int8_t*>(&nextseq));
		int8_t* dest (reinterpret_cast<int8_t*>(&data_));
		for(unsigned i=0;i<8;i++){
		        seq_ptr1++;
			*(dest++) = row[*(seq_ptr1++)];
		}
		for(unsigned i=0;i<8;i++){
		        seq_ptr2++;
		        *(dest++) = row[*(seq_ptr2++)];
		}
        }

	
	score_vector2 first(const score_vector2 &rhs)
	{
#ifdef __SSSE3__
	        __m128i const zero = _mm_setzero_si128();
		data_ = _mm_srai_epi16(_mm_unpacklo_epi8(zero, rhs.data_), 8);
		return *this;
#endif
	}
	
	score_vector2 first_generic(const score_vector2 &rhs)
	{
	        const int8_t* seq_ptr (reinterpret_cast<const int8_t*>(&rhs.data_));
		int8_t* dest (reinterpret_cast<int8_t*>(&data_));
		for(unsigned i=0;i<8;i++){
		        *dest = *seq_ptr;
			dest += 2;
			seq_ptr++;
		}
		for(unsigned i=0;i<8;i++){
		        *dest = *dest * 256;
			dest += 2;
		}
		return *this;
	}

        score_vector2 second(const score_vector2 &rhs)
	{
#ifdef __SSSE3__
	        __m128i const zero = _mm_setzero_si128();
		data_ = _mm_srai_epi16(_mm_unpackhi_epi8(zero, rhs.data_), 8);
		return *this;
#endif
	}

	score_vector2 second_generic(const score_vector2 &rhs)
        {
	        const int8_t* seq_ptr (reinterpret_cast<const int8_t*>(&rhs.data_));
		int8_t* dest (reinterpret_cast<int8_t*>(&data_));
		seq_ptr += 8;
                for(unsigned i=0;i<8;i++){
		        *dest = *seq_ptr;
			dest += 2;
			seq_ptr++;
                }
		for(unsigned i=0;i<8;i++){
		        *dest = *dest * 256;
			dest += 2;
		}
		return *this;
	}

	score_vector2(const int16_t* s):
		data_ (_mm_loadu_si128(reinterpret_cast<const __m128i*>(s)))
	{ }

	score_vector2 operator+(const score_vector2 &rhs) const
	{
		return score_vector2 (_mm_add_epi16(data_, rhs.data_));
	}

	score_vector2 operator-(const score_vector2 &rhs) const
	{
		return score_vector2 (_mm_sub_epi16(data_, rhs.data_));
	}

	score_vector2& operator-=(const score_vector2 &rhs)
	{
		data_ = _mm_sub_epi16(data_, rhs.data_);
		return *this;
	}

	//void unbias(const score_vector2 &bias)
	//{ this->operator -=(bias); }

	void p()
	{
	  //return *(((int16_t*)&data_)+i);
	        print(data_);
	}

	int operator [](unsigned i) const
        {
	  /*int value;
	  int16_t *pt = (int16_t*)&data_;
	  for(unsigned j=0; j<i; j++)
	    value = (int)*(pt++);
	  return value;*/
	  return *(((int16_t*)&data_)+i);
	}

	void set(unsigned i, int16_t v)
	{
	        *(((int16_t*)&data_)+i) = v;
	}

	score_vector2& max(const score_vector2 &rhs)
	{
		data_ = _mm_max_epi16(data_, rhs.data_);
		return *this;
	}

	score_vector2& min(const score_vector2 &rhs)
	{
		data_ = _mm_min_epi16(data_, rhs.data_);
		return *this;
	}

	score_vector2& update_row(const score_vector2 &rhs, const score_vector2 &row, score_vector2 &row_best)
	{
	        __m128i temp = _mm_cmplt_epi16(data_, rhs.data_);	//(a0 < b0) ? 0xffff : 0x0
		__m128i select_row = _mm_and_si128(row.data_, temp);	//bitwise and
		//data_[i] < rhs.data_[i] ? row.data_[i] : 0
		__m128i temp2 = _mm_xor_si128(temp, _mm_set1_epi16(0xffff));	// = Not
		__m128i origin_row = _mm_and_si128(row_best.data_, temp2);
		//origin_row[i] = data_[i] >= rhs.data_[i] ? row_best.data_[i] : 0
		row_best.data_ = _mm_or_si128(select_row, origin_row);
		/* 	if rhs.data_[i] > data_[i] : row_best.data_[i] = row.data_[i] 
			else if rhs.data_[i] <= data_[i] : row_best.data_[i] unchange */

		data_ = _mm_max_epi16(data_, rhs.data_);
		return *this;
	}
	
	score_vector2& update(const score_vector2 &rhs, const score_vector2 &row, const score_vector2 &column, score_vector2 &row_best, score_vector2 &column_best)
	{
	        __m128i temp = _mm_cmplt_epi16(data_, rhs.data_);
		__m128i select_row = _mm_and_si128(row.data_, temp);
		__m128i select_column = _mm_and_si128(column.data_, temp);
		__m128i temp2 = _mm_xor_si128(temp, _mm_set1_epi16(0xffff));
		__m128i origin_row = _mm_and_si128(row_best.data_, temp2);
		__m128i origin_column = _mm_and_si128(column_best.data_, temp2);
		row_best.data_ = _mm_or_si128(select_row, origin_row);
		column_best.data_ = _mm_or_si128(select_column, origin_column);
		data_ = _mm_max_epi16(data_, rhs.data_);
		return *this;
	}

	friend score_vector2 max(const score_vector2& lhs, const score_vector2 &rhs)
	{
		return score_vector2 (_mm_max_epi16(lhs.data_, rhs.data_));
	}
	
	friend score_vector2 min(const score_vector2& lhs, const score_vector2 &rhs)
	{
		return score_vector2 (_mm_min_epi16(lhs.data_, rhs.data_));
	}

	/*int16_t cmpeq(const score_vector2 &rhs) const
	{
		return _mm_movemask_epi8(_mm_cmpeq_epi16(data_, rhs.data_));
	}

	__m128i cmpeq2(const score_vector2 &rhs) const
	{
		return _mm_cmpeq_epi16(data_, rhs.data_);
	}

	int16_t cmpgt(const score_vector2 &rhs) const
	{
		return _mm_movemask_epi8(_mm_cmpgt_epi16(data_, rhs.data_));
	}*/
	
	score_vector2 cmpgt(const score_vector2 &rhs) const
        {
	        return score_vector2 (_mm_cmpgt_epi16(data_, rhs.data_));
        }


    //新增的函数
    score_vector2 cmplt(const score_vector2 &rhs) const
    {
	    return score_vector2 (_mm_cmplt_epi16(data_, rhs.data_));
    }

    score_vector2 abs(){
    	return score_vector2 (_mm_abs_epi16(data_));
    }

    //按位与
    score_vector2 vand(const score_vector2 &rhs){
    	return score_vector2 (_mm_and_si128(data_, rhs.data_));
    }

    //按位或
    score_vector2 vor(const score_vector2 &rhs){
    	return score_vector2 (_mm_or_si128(data_, rhs.data_));
    }

    //0xffff -> 1, 0 -> 0
    score_vector2 mask(){
    	__m128i value =  _mm_set1_epi16(1);
    	return score_vector2 (_mm_and_si128(value, data_));
    }

    void operator--(){
    	data_ = _mm_sub_epi16(data_, _mm_set1_epi16(1));
    }

    void operator++(){
    	data_ = _mm_add_epi16(data_, _mm_set1_epi16(1));
    }

	score_vector2& operator+=(const score_vector2 &rhs)
	{
		data_ = _mm_add_epi16(data_, rhs.data_);
		return *this;
	}

	score_vector2 operator*(const score_vector2 &rhs) const
	{
		return score_vector2 (_mm_mullo_epi16(data_, rhs.data_));
	}

	//向量中所有元素均为0
    bool allzero(){
		__m128i zero = _mm_setzero_si128();
		__m128i ones = _mm_cmpeq_epi32(zero, zero);

		//SSE4的新增指令
		if (_mm_testz_si128(data_, ones) == 1) return true;
		return false;
    }

    score_vector2 cmpeq(const score_vector2 &rhs) const {
    	return score_vector2 (_mm_cmpeq_epi16(data_, rhs.data_));
    }

	__m128i data_;

};

#endif /* SCORE_VECTOR2_H_ */
