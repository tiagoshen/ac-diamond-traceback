#ifndef TRACEBACK3_H_
#define TRACEBACK3_H_

#include "score_vector2.h"
#include <iostream>
#include "../util/tinythread.h"
tthread::mutex o_mutex;

const __m128i zero = _mm_setzero_si128();
const __m128i ones = _mm_cmpeq_epi16(zero, zero);
const __m128i one = _mm_set1_epi16(1);


inline bool allzero(const __m128i &value) {
	return _mm_movemask_epi8(_mm_cmpeq_epi32(value, zero)) == 0xFFFF;
}

template<typename _score>
struct Traceback_matrix
{
private:
	score_vector2<_score> *scores_;
	__m128i band_;
    __m128i width_;
    int ref_len;
public:
        Traceback_matrix(score_vector2<_score> *scores, int band, int len){
        	scores_ = scores;
        	band_ = _mm_set1_epi16(band+1);
        	width_ = _mm_set1_epi16(2*band+2);
        	ref_len = len;
        }

		
		//根据col和row从scores中返回新的score_vector2
        __m128i operator()(__m128i &col, __m128i &row){
	  		//对应原来的代码：int top_delta (col > band_ ? col - band_ : 0);
        	__m128i top_delta = _mm_sub_epi16(col, band_);
	  		__m128i temp = _mm_cmpgt_epi16(col, band_);
	  		top_delta = _mm_and_si128(temp, top_delta);
	  		//((col - band).cmpgt(zero).mask())*(col-band);

	  		//mask：相当于一个boolean vector, true时为1，false时为0
	  		//in_band
	  		__m128i inband = this->in_band(col, row);

	  		//对应原来的代码：int pos = col*width_+row-top_delta;
	  		__m128i pos = _mm_mullo_epi16(col, width_);
	  		pos = _mm_add_epi16(pos, row);
	  		pos = _mm_sub_epi16(pos, top_delta);
	  		pos = _mm_and_si128(pos, inband);

	  		//需要返回的score_vector2<_score>,这里将sv中的每个元素单独赋值，没有用到SIMD
	  		__m128i re = _mm_setzero_si128();
	  		for (int id=0; id<ref_len; id++){
	  			if (*(((int16_t*)&pos)+id) <= 0) *(((int16_t*)&re)+id) = 0;
	  			else *(((int16_t*)&re)+id) = scores_[*(((int16_t*)&pos)+id)][id];
	  		}
	  		
	  		return re;
		}

        //返回的是boolean vector，true是1，false是0
		__m128i in_band(__m128i &col, __m128i &row){
			//原来的代码：return abs(col - row) < band_ && row > 0 && col > 0;
			__m128i t1 = _mm_cmplt_epi16(_mm_abs_epi16(_mm_sub_epi16(col, row)), band_);
			__m128i t2 = _mm_and_si128(_mm_cmpgt_epi16(row, zero), _mm_cmpgt_epi16(col, zero));
			return _mm_and_si128(t1, t2);
		}
};

template<typename _score>
//i代表各自的行，j代表各自的列，返回boolean vector
__m128i have_vgap(Traceback_matrix<_score> &dp,
	       __m128i &i,				//i在调用过程中有改动，应该拷贝值
	       __m128i &j,				//j不改动,所以不用拷贝
	       __m128i &gap_open,		//8个都一样
	       __m128i &gap_extend,		//8个都一样	
	       __m128i &l,					//gap_len
	       int band)									
{
    __m128i score = dp(j, i);
    
    //将向量l中的每个元素赋值为1，对应非SIMD的逻辑：l = 1
	l = _mm_set1_epi16(1);

	//r是i的拷贝
	//r--;
	__m128i r = _mm_sub_epi16(i, one);
	
	__m128i pattern = _mm_setzero_si128();
	while(true) {
	    
	    //对应非SIMD的逻辑: while(dp.in_band(j, i))
	    __m128i bpattern = dp.in_band(j, r);
	    if (allzero(bpattern)) break;
	    
	    //对应非SIMD的逻辑:
	    //if((int)score == (int)dp(j, r) - gap_open - (l-1)*gap_extend)
		//return true;
		__m128i temp = _mm_sub_epi16(_mm_sub_epi16(dp(j, r), gap_open), _mm_mullo_epi16(_mm_sub_epi16(l, one), gap_extend));
		temp = _mm_cmpeq_epi16(score, temp);
		temp = _mm_and_si128(temp, bpattern);
		pattern = _mm_or_si128(temp, pattern);
		//mask = score.cmpeq(dp(j, r) - gap_open - (l-one)*gap_extend).vand(bmask).vor(mask);
		r = _mm_sub_epi16(r, _mm_and_si128(one, bpattern));
		l = _mm_add_epi16(l, _mm_and_si128(one, bpattern));
	}
	return pattern;
}


template<typename _score>
//i代表各自的行，j代表各自的列，返回boolean vector
__m128i have_hgap(Traceback_matrix<_score> &dp,
	       __m128i &i,				//i不改动,所以不用拷贝
	       __m128i &j,				//j在调用过程中有改动，应该拷贝值
	       __m128i &gap_open,		//8个都一样
	       __m128i &gap_extend,		//8个都一样	
	       __m128i &l,					//gap_len
	       int band)
{
    __m128i score = dp(j, i);

    //将向量l中的每个元素赋值为1，对应非SIMD的逻辑：l = 1
	l = _mm_set1_epi16(1);

	//r是j的拷贝
	__m128i r = _mm_sub_epi16(j, one);

	__m128i pattern = _mm_setzero_si128();
	while(true) {
		
		//对应非SIMD的逻辑: while(dp.in_band(j, i))
		__m128i bpattern = dp.in_band(r, i);
		if (allzero(bpattern)) break;

		__m128i temp = _mm_sub_epi16(_mm_sub_epi16(dp(r, i), gap_open), _mm_mullo_epi16(_mm_sub_epi16(l, one), gap_extend));
		temp = _mm_cmpeq_epi16(score, temp);
		temp = _mm_and_si128(temp, bpattern);
		pattern = _mm_or_si128(temp, pattern);
	    /*mask = score.cmpeq(dp(r, i) - gap_open - (l-one)*gap_extend).vand(bmask).vor(mask)
		--r;
		++l;*/
		r = _mm_sub_epi16(r, _mm_and_si128(one, bpattern));
		l = _mm_add_epi16(l, _mm_and_si128(one, bpattern));
	}
	return pattern;
}

template<typename _val, typename _score>
void dp_traceback_right(vector<local_match<_val> > &segment,
			vector<sequence<const _val> > &ref_suffix_set,
			int n,
			sequence<const _val> &query_suffix,
			score_vector2<_score> *scores,	//floating_sw2.h: traceback
			int reflen,						//floating_sw2.h: ref_len
			__m128i &i,		//floating_sw2.h: best_row
			__m128i &j,		//floating_sw2.h: best_column
			__m128i &best,	//floating_sw2.h: best

			int gap_open,
			int gap_extend,
			int scalar_band,				//band

			int begin,						//floating_sw2.h: begin
			vector<char> &transcript_buf)
{
	if(reflen>8 || reflen<=0) return;
    
    //Constants
    __m128i gopen = _mm_set1_epi16(gap_open);
    __m128i gextend = _mm_set1_epi16(gap_extend);
	Traceback_matrix<_score> dp (scores, scalar_band, reflen);
	
	//Build segment attributes into vectors
	/*
  	segment.query_len_ = i + 1;
  	segment.subject_len_ = j + 1;
  	segment.query_begin_ = 0;
  	segment.subject_begin_ = 0;
  	segment.score_ = best;
  	*/
  	__m128i query_len = _mm_add_epi16(i,one);
  	__m128i subject_len = _mm_add_epi16(j,one);
  	__m128i query_begin = _mm_setzero_si128();
  	__m128i subject_begin = _mm_setzero_si128();
  	__m128i score = best;
  	__m128i identities = _mm_setzero_si128();
  	__m128i mismatches = _mm_setzero_si128();
  	__m128i len = _mm_setzero_si128();
  	__m128i gap_openings = _mm_setzero_si128();
 
	__m128i gap_len = _mm_setzero_si128();

	//0对应op_insertion, a>0对应op_insertion, a = gap_len, b<0对应op_deletion, |b| = gap_len 
	vector<vector<short> > operations (reflen);
	
	i = _mm_add_epi16(one, i);
	j = _mm_add_epi16(one, j);

	bool endloop = false;
	__m128i ap = ones;
	while(!endloop) {		//i>1 || j>1
	    //需要读取所有match_score
	   	__m128i match_score = _mm_setzero_si128();
	    for (int id=0; id<reflen; id++){
	    	if(*(((int16_t*)&ap)+id) == -1)
	    	*(((int16_t*)&match_score)+id) = score_matrix::get().letter_score(query_suffix[*(((int16_t*)&i)+id)-1], mask_critical(ref_suffix_set[id+n][*(((int16_t*)&j)+id)-1]));
	    }



	    //主要流程
	    //创建一个以以上条件判断为结果的boolean vector (mask)，true为1，false为0
	    //对应非SIMD逻辑：if(dp(j, i) == (_score)match_score + dp(j-1, i-1))
		__m128i pattern1 = dp(j, i);
		__m128i j1 = _mm_sub_epi16(j, one);
		__m128i i1 = _mm_sub_epi16(i, one);
		__m128i sub = dp(j1, i1);
		__m128i temp = _mm_add_epi16(match_score, sub);
		pattern1 = _mm_cmpeq_epi16(pattern1, temp);
		pattern1 = _mm_and_si128(ap, pattern1);
		__m128i mask1 = _mm_and_si128(one, pattern1);

			  __m128i ident = _mm_setzero_si128();
			  //对应非SIMD逻辑：if(query_suffix[i-1] == mask_critical(ref_suffix_set[j-1]))
			  for (int id=0; id<reflen; id++){
			  	if(*(((int16_t*)&ap)+id) == -1){
		      		if(query_suffix[*(((int16_t*)&i)+id)-1] == mask_critical(ref_suffix_set[id+n][*(((int16_t*)&j)+id)-1]))
		        		*(((int16_t*)&ident)+id) = 1;
		    	}
		      }
		      identities = _mm_add_epi16(identities, ident);
		      mismatches = _mm_add_epi16(mismatches, _mm_and_si128(_mm_sub_epi16(one, ident), ap));
			  i = _mm_sub_epi16(i, mask1);	//i--
			  j = _mm_sub_epi16(j, mask1);	//j--
			  len = _mm_add_epi16(len, mask1); //segment.len_++
			  
			  //只能通过for循环来添加信息，无法通过SIMD完成这个任务
			  //transcript_buf.push_back(op_match);
			  for (int id=0; id<reflen; id++){
			  	if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask1)+id) == 1) operations[id].push_back(0);
			  	}
			  }

		//对应非SIMD逻辑：else if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, band)) 
		__m128i pattern2 = have_hgap(dp, i, j, gopen, gextend, gap_len, scalar_band);
		pattern2 = _mm_and_si128(ap, pattern2);
		__m128i mask2 = _mm_and_si128(one, pattern2);

			gap_openings = _mm_add_epi16(gap_openings, mask2);
			len = _mm_add_epi16(_mm_and_si128(gap_len, pattern2), len);
			j = _mm_sub_epi16(j, _mm_and_si128(gap_len, pattern2));
			for (int id=0; id<reflen; id++){
				if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask2)+id) == 1) operations[id].push_back(-(*(((int16_t*)&gap_len)+id)));
			  	}
			}

		__m128i pattern3 = have_vgap(dp, i, j, gopen, gextend, gap_len, scalar_band);
		pattern3 = _mm_and_si128(ap, pattern3);
		__m128i mask3 = _mm_and_si128(one, pattern3);

			gap_openings = _mm_add_epi16(gap_openings, mask3);
			len = _mm_add_epi16(_mm_and_si128(gap_len, pattern3), len);
			i = _mm_sub_epi16(i, _mm_and_si128(gap_len, pattern3));
			for (int id=0; id<reflen; id++){
				if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask3)+id) == 1) operations[id].push_back(*(((int16_t*)&gap_len)+id));
			  	}
			}

		//else throw std::runtime_error("Traceback error.");
		__m128i patternextra = _mm_or_si128(pattern1, pattern2);
		patternextra = _mm_or_si128(patternextra, pattern3);

		i = _mm_and_si128(i, patternextra);
		j = _mm_and_si128(j, patternextra);
		
		//while(i>1 || j>1)
		i = _mm_and_si128(i, _mm_cmpgt_epi16(i, one));
		j = _mm_and_si128(j, _mm_cmpgt_epi16(i, one));
		ap = _mm_or_si128(_mm_cmpgt_epi16(i, one), _mm_cmpgt_epi16(j, one));
		endloop = allzero(ap);

	}
	
	__m128i mask4 = _mm_setzero_si128();
	for (int id=0; id<reflen; id++){
		if(query_suffix[0] == mask_critical(ref_suffix_set[id+n][0]))
		    *(((int16_t*)&mask4)+id)=1;
	}
	identities = _mm_add_epi16(identities, mask4);
	mismatches = _mm_add_epi16(mismatches, _mm_sub_epi16(one, mask4));
	len = _mm_add_epi16(len, one);
	for (int id=0; id<reflen; id++){
		operations[id].push_back(0);
	}

	
	//将所有输出数据汇总
	for (int id=0; id<reflen; id++){
		Edit_transcript transcript (transcript_buf);
		segment[id+begin].query_len_ = *(((int16_t*)&query_len)+id);
  		segment[id+begin].subject_len_ = *(((int16_t*)&subject_len)+id);
  		//No use?
  		segment[id+begin].query_begin_ = *(((int16_t*)&query_begin)+id);
  		segment[id+begin].subject_begin_ = *(((int16_t*)&subject_begin)+id);
 		segment[id+begin].score_ = *(((int16_t*)&score)+id);
  		segment[id+begin].identities_ += *(((int16_t*)&identities)+id);
  		segment[id+begin].mismatches_ += *(((int16_t*)&mismatches)+id);
  		segment[id+begin].len_ += *(((int16_t*)&len)+id);
  		segment[id+begin].gap_openings_ += *(((int16_t*)&gap_openings)+id);
  		for(vector<short int>::iterator it=operations[id].begin();it!=operations[id].end();++it){
			if (*it==0){
				transcript_buf.push_back(op_match);
			}else if (*it>0){
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_insertion);
			}else{
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_deletion);
			}
		}
		//printf("len=%i\n",segment.len_);
		segment[id+begin].transcript_right_ = transcript.set_end(transcript_buf);
	}
}


//未改

template<typename _val, typename _score>
void dp_traceback_left(vector<local_match<_val> > &segment,
		       vector<sequence<const _val> > &ref_prefix_set,
		       int n,
			   sequence<const _val> &query_prefix,
			   score_vector2<_score> *scores,	//在floating_sw2.h中是traceback
			   int reflen,						//替换掉num，这里reflen是reference sequence的"数量"1-8个
			   __m128i &i,		//在floating_sw2.h中是best_row
			   __m128i &j,		//在floating_sw2.h中是best_column
			   __m128i &best,	//在floating_sw2.h中是best
			   //这一部分没有变
			   int gap_open,
			   int gap_extend,
			   int scalar_band,				//原来是band,改了名字

			   int begin,						//在floating_sw2.h中是begin用来取segment中相应位置的元素
			   vector<char> &transcript_buf)
{
	if(reflen>8 || reflen<=0) return;

	//Build segment_left attributes into vectors
    __m128i gopen = _mm_set1_epi16(gap_open);
    __m128i gextend = _mm_set1_epi16(gap_extend);
	Traceback_matrix<_score> dp (scores, scalar_band, reflen);
	
	__m128i query_len = _mm_add_epi16(i,one);
  	__m128i subject_len = _mm_add_epi16(j,one);
  	__m128i query_begin = _mm_setzero_si128();
  	__m128i subject_begin = _mm_setzero_si128();
  	__m128i score = best;
  	__m128i identities = _mm_setzero_si128();
  	__m128i mismatches = _mm_setzero_si128();
  	__m128i len = _mm_setzero_si128();
  	__m128i gap_openings = _mm_setzero_si128();
 
	__m128i gap_len = _mm_setzero_si128();

	//0对应op_insertion, a>0对应op_insertion, a = gap_len, b<0对应op_deletion, |b| = gap_len 
	vector<vector<short int> > operations (reflen);
	
	i = _mm_add_epi16(one, i);
	j = _mm_add_epi16(one, j);

	__m128i slen = _mm_setzero_si128();
	for (int id=0; id<reflen; id++){
	 *(((int16_t*)&slen+id)) = ref_prefix_set[id+n].length();
	}
	int qlen = query_prefix.length();
	bool endloop = false;
	__m128i ap = ones;
	while(!endloop) {		//i>1 || j>1
	    //需要读取所有match_score
	   	__m128i match_score = _mm_setzero_si128();
	    for (int id=0; id<reflen; id++){
	    	if(*(((int16_t*)&ap)+id) == -1)
	    	*(((int16_t*)&match_score)+id) = score_matrix::get().letter_score(query_prefix[qlen-*(((int16_t*)&i)+id)], mask_critical(ref_prefix_set[id+n][*(((int16_t*)&slen)+id) - (*(((int16_t*)&j)+id))]));
	    }

	    //主要流程
	    //创建一个以以上条件判断为结果的boolean vector (mask)，true为1，false为0
	    //对应非SIMD逻辑：if(dp(j, i) == (_score)match_score + dp(j-1, i-1))
		__m128i pattern1 = dp(j, i);
		__m128i j1 = _mm_sub_epi16(j, one);
		__m128i i1 = _mm_sub_epi16(i, one);
		__m128i sub = dp(j1, i1);
		__m128i temp = _mm_add_epi16(match_score, sub);
		pattern1 = _mm_cmpeq_epi16(pattern1, temp);
		pattern1 = _mm_and_si128(pattern1, ap);
		__m128i mask1 = _mm_and_si128(one, pattern1);

			  __m128i ident = _mm_setzero_si128();
			  //对应非SIMD逻辑：if(query_suffix[i-1] == mask_critical(ref_suffix_set[j-1]))
			  for (int id=0; id<reflen; id++){
			  	if(*(((int16_t*)&ap)+id) == -1){
		      		if(query_prefix[qlen-*(((int16_t*)&i)+id)] == mask_critical(ref_prefix_set[id+n][*(((int16_t*)&slen)+id) - (*(((int16_t*)&j)+id))]))
		        		*(((int16_t*)&ident)+id) = 1;
		        }
		      }
		      identities = _mm_add_epi16(identities, ident);
		      /*o_mutex.lock();
		      for(int id=0; id<reflen; id++){
		      	cout <<"identities["<<id<<"]: "<<*(((int16_t*)&identities)+id);
		      }
		      o_mutex.unlock();*/
		      mismatches = _mm_add_epi16(mismatches, _mm_and_si128(_mm_sub_epi16(one, ident), ap));
			  i = _mm_sub_epi16(i, mask1);	//i--
			  j = _mm_sub_epi16(j, mask1);	//j--
			  len = _mm_add_epi16(len, mask1); //segment.len_++
			  
			  //只能通过for循环来添加信息，无法通过SIMD完成这个任务
			  //transcript_buf.push_back(op_match);
			  for (int id=0; id<reflen; id++){
			  	if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask1)+id) == 1) operations[id].push_back(0);
			  	}
			  }
		//对应非SIMD逻辑：else if (have_hgap(dp, i, j, gap_open, gap_extend, gap_len, band)) 
		__m128i pattern2 = have_hgap(dp, i, j, gopen, gextend, gap_len, scalar_band);
		pattern2 = _mm_and_si128(ap, pattern2);
		__m128i mask2 = _mm_and_si128(one, pattern2);

			gap_openings = _mm_add_epi16(gap_openings, mask2);
			len = _mm_add_epi16(_mm_and_si128(gap_len, pattern2), len);
			j = _mm_sub_epi16(j, _mm_and_si128(gap_len, pattern2));
			for (int id=0; id<reflen; id++){
				if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask2)+id) == 1) operations[id].push_back(-(*(((int16_t*)&gap_len)+id)));
			  	}
			}

		__m128i pattern3 = have_vgap(dp, i, j, gopen, gextend, gap_len, scalar_band);
		pattern3 = _mm_and_si128(ap, pattern3);
		__m128i mask3 = _mm_and_si128(one, pattern3);

			gap_openings = _mm_add_epi16(gap_openings, mask3);
			len = _mm_add_epi16(_mm_and_si128(gap_len, pattern3), len);
			i = _mm_sub_epi16(i, _mm_and_si128(gap_len, pattern3));
			for (int id=0; id<reflen; id++){
				if(*(((int16_t*)&ap)+id) == -1){
			  		if (*(((int16_t*)&mask3)+id) == 1) operations[id].push_back(*(((int16_t*)&gap_len)+id));
			  	}
			}

		//else throw std::runtime_error("Traceback error.");
		__m128i patternextra = _mm_or_si128(pattern1, pattern2);
		patternextra = _mm_or_si128(patternextra, pattern3);

		i = _mm_and_si128(i, patternextra);
		j = _mm_and_si128(j, patternextra);
		
		//while(i>1 || j>1)
		i = _mm_and_si128(i, _mm_cmpgt_epi16(i, one));
		j = _mm_and_si128(j, _mm_cmpgt_epi16(i, one));
		ap = _mm_or_si128(_mm_cmpgt_epi16(i, one), _mm_cmpgt_epi16(j, one));
		endloop = allzero(ap);
	}

	__m128i mask4 = _mm_setzero_si128();
	for (int id=0; id<reflen; id++){
		if(query_prefix[qlen-1] == mask_critical(ref_prefix_set[id+n][*(((int16_t*)&slen)+id) - 1]))
		    *(((int16_t*)&mask4)+id)=1;
	}
	identities = _mm_add_epi16(identities, mask4);
	mismatches = _mm_add_epi16(mismatches, _mm_sub_epi16(one, mask4));
	len = _mm_add_epi16(len, one);
	for (int id=0; id<reflen; id++){
		operations[id].push_back(0);
	}

	vector<local_match<_val> > segment_left (reflen);
	//将所有输出数据汇总
	for (int id=0; id<reflen; id++){
		/*o_mutex.lock();
		cout <<"identities of "<<id<<": "<<*(((int16_t*)&identities)+id) <<endl;
		o_mutex.unlock();*/
		Edit_transcript transcript (transcript_buf);
		segment_left[id].query_len_ = *(((int16_t*)&query_len)+id);
  		segment_left[id].subject_len_ = *(((int16_t*)&subject_len)+id);
  		//No use?
  		segment_left[id].query_begin_ = *(((int16_t*)&query_begin)+id);
  		segment_left[id].subject_begin_ = *(((int16_t*)&subject_begin)+id);

  		segment_left[id].score_ = *(((int16_t*)&score)+id);
  		segment_left[id].identities_ += *(((int16_t*)&identities)+id);
  		segment_left[id].mismatches_ += *(((int16_t*)&mismatches)+id);
  		segment_left[id].len_ += *(((int16_t*)&len)+id);
  		segment_left[id].gap_openings_ += *(((int16_t*)&gap_openings)+id);
  		for(vector<short int>::iterator it=operations[id].begin();it!=operations[id].end();++it){
			if (*it==0){
				transcript_buf.push_back(op_match);
			}else if (*it>0){
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_insertion);
			}else{
				transcript_buf.insert(transcript_buf.end(), (int) *it, op_deletion);
			}
		}

		segment_left[id].transcript_right_ = transcript.set_end(transcript_buf);
		//segment[id+begin] -= segment_left[id];
		segment[id+begin].len_ += segment_left[id].len_;
		segment[id+begin].subject_len_ += segment_left[id].subject_len_;
		segment[id+begin].gap_openings_ += segment_left[id].gap_openings_;
		segment[id+begin].identities_ += segment_left[id].identities_;
		segment[id+begin].mismatches_ += segment_left[id].mismatches_;
		segment[id+begin].score_ += segment_left[id].score_;
		segment[id+begin].query_len_ += segment_left[id].query_len_;
		
		segment[id+begin].query_begin_ = segment_left[id].query_len_;
		//Problem: 
		o_mutex.lock();
		cout <<"segment["<<id+begin<<"].subject_begin_ = "<<segment[id+begin].subject_begin_<<"  AND  ";
		cout <<"segment_left["<<id<<"].subject_len_ = "<<segment_left[id].subject_len_<<endl;
		o_mutex.unlock();
		segment[id+begin].subject_begin_ = segment_left[id].subject_len_;
		segment[id+begin].transcript_left_ = segment_left[id].transcript_right_;
		


		segment[id+begin].query_begin_--;
		segment[id+begin].subject_begin_--;
		
		segment[id+begin].score_ -= score_matrix::get().letter_score(query_prefix[qlen-1], mask_critical(ref_prefix_set[id+n][*(((int16_t*)&slen)+id) - 1]));
		if (query_prefix[qlen-1] == mask_critical(ref_prefix_set[id+n][*(((int16_t*)&slen)+id) - 1]))
	        segment[id+begin].identities_--;
		else
	        segment[id+begin].mismatches_--;
		segment[id+begin].len_--;
		segment[id+begin].subject_len_--;
		segment[id+begin].query_len_--;
	}
}

/*template<typename _val, typename _score>
void dp_traceback(const _score &scores,
	     int gap_open,
	     int gap_extend,
	     int i,
	     int j,
	     int score)
{ return local_match<_val> (score); }
*/
#endif /* TRACEBACK3_H_ */